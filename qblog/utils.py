import random

rand_eng = random.SystemRandom()

charset = 'abcdefghjkmnopqrstuvwxyz23456789'

def normalize():
    raise NotImplementedError

def random_str(chars=None, length=5, length_to=None):
    """Returns ``length`` long sequence of random picks from chars.
    If ``length_to`` is defined, random length between ``length`` and
    ``length_to`` is chosen.
    """
    if chars is None:
        chars = charset
    if length_to is not None:
        length = rand_eng.randint(length, length_to)
    return ''.join((rand_eng.choice(chars) for i in range(length)))

