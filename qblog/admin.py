from django.contrib import admin
from django import forms

from .models import Article

class ArticleAdminForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(attrs={
            'rows': 30,
            'cols': 80,
            'style': 'font-family: monospace',
            }),
        help_text='<a href="http://docutils.sourceforge.net/docs/user/rst/quickref.html">reStructuredText Quick Reference</a>'
        )

    class Meta:
        model = Article
        fields = ['title', 'slug', 'link', 'abstract', 'text', 'index', 'date']

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
    prepopulated_fields = {"slug": ("title",)}

admin.site.register(Article, ArticleAdmin)

