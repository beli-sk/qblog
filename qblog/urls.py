from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from django.conf import settings

from . import views

urlpatterns = []

urlpatterns += patterns('qblog.views',
    url(r'^a/(?P<slug>.*)$', views.Article.as_view(), name='qblog-article'),
    url(r'^$', views.Home.as_view(), name='qblog-home'),
    #url(r'^$', RedirectView.as_view(pattern_name='qblog-index', permanent=False)),
    )
