
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.generic import View

from . import models
from . import widgets

import logging
logger = logging.getLogger(__name__)

class Article(View):
    def get(self, request, slug):
        return render(request, 'qblog/article.html', {
            'article': widgets.Article(request, models.Article.objects.get(slug=slug))
            })

class Index(View):
    template_name = 'qblog/index.html'
    def get(self, request):
        return render(request, self.template_name, {
            'abstracts': [widgets.Abstract(request, x) for x in models.Article.objects.filter(index=True)],
            })

class Home(Index):
    template_name = 'qblog/home.html'

