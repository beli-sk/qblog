# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=512)),
                ('slug', models.CharField(max_length=128)),
                ('link', models.CharField(max_length=8)),
                ('abstract', models.TextField(max_length=1024)),
                ('text', models.TextField(max_length=8192)),
                ('date', models.DateTimeField(max_length=1024)),
            ],
        ),
    ]
