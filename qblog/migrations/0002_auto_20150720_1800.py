# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qblog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='date',
            field=models.DateTimeField(db_index=True, max_length=1024),
        ),
        migrations.AlterField(
            model_name='article',
            name='link',
            field=models.CharField(db_index=True, max_length=8),
        ),
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(max_length=64),
        ),
    ]
