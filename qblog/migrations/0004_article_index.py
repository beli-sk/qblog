# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qblog', '0003_auto_20150720_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='index',
            field=models.BooleanField(default=True),
        ),
    ]
