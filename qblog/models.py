import re
from docutils.core import publish_parts, publish_string

from django.db import models

from . import utils

ABSTRACT_SEPARATOR = '/...'
ABSTRACT_LENGTH = 200

def truncate_words(s, length):
    return re.sub(r'\W+\w*$', r'', s[:length+1])

class Article(models.Model):
    title = models.CharField(max_length=512)
    slug = models.SlugField(max_length=64, db_index=True, unique=True)
    link = models.CharField(max_length=8, db_index=True, blank=True, unique=True)
    abstract = models.TextField(max_length=1024, blank=True)
    text = models.TextField(max_length=8192)
    date = models.DateTimeField(max_length=1024, db_index=True)
    index = models.BooleanField(default=True, db_index=True)

    def clean(self):
        if not self.link:
            self.link = utils.random_str()
        if not self.abstract:
            if ABSTRACT_SEPARATOR in self.text:
                self.abstract = self.text.split(ABSTRACT_SEPARATOR, 1)[0]
            else:
                self.abstract = truncate_words(self.text, ABSTRACT_LENGTH) + '&hellip;'
        super().clean()

    def __str__(self):
        return self.title

    def publish_raw(self):
        return publish_parts(self.text, writer_name='html')['html_body']

    def publish(self):
        return self.publish_raw().replace(ABSTRACT_SEPARATOR, '', 1)

