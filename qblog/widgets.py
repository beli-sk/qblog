from django.template import RequestContext, loader

def render_str(request, template_file, context={}):
    template = loader.get_template(template_file)
    context = RequestContext(request, context)
    return template.render(context)

class Home(object):
    def render(self, template=None):
        if template is None:
            template = 'home.html'
        return render_str(self.request, template)

class Article(object):
    def __init__(self, request, article):
        self.request = request
        self.article = article

    def render(self, template=None):
        if template is None:
            template = 'qblog/widgets/article.html'
        return render_str(self.request, template, {'article': self.article})

class Abstract(Article):
    def render(self):
        return super().render(template='qblog/widgets/abstract.html')

