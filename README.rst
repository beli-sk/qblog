QBlog
=====

Simple blog app powered by Django web framework.

Install
-------

Install requirements.

Include qblog.urls in urls.py.

Add qblog to installed apps.

Install migrations.
